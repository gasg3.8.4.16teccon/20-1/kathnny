from kathnny_consultas import *
import json

def conjugacionRespuesta(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :conjugacionRespuesta"
	resultado = { 'palabraTraducida' : None, 'definicion': None, 'conjugaciones': None}
	palabraTraducida = str(llamadaAPI('translate','translate', palabra))
	definicion = llamadaAPI('words_api','definitions', palabraTraducida)
	definicion_j = json.loads(definicion)
	conjugaciones = llamadaAPI('linguatool_conjugations','conjugate', palabraTraducida)
	conjugaciones_j = json.loads(conjugaciones)
	apiIngles = llamadaAPI('dolphinWords','buscarDic', palabraTraducida)
	resultado['palabraTraducida'] = palabraTraducida
	if (('message' in definicion_j.keys()) and ('word not found' in definicion_j['message'])):
		resultado['definicion'] = 'word not found'
	else:
		resultado['definicion'] = definicion_j['definitions']
	if 'nicht im Lexikon' in conjugaciones_j['result']:
		resultado['conjugaciones'] = 'word not found'
	else:
		resultado['conjugaciones'] = conjugaciones_j['conjugated_forms']
	resultado['apiIngles'] = apiIngles
	return resultado

def traduccion(palabra):
	assert isinstance(palabra, str), "solo palabras porfavor ver funcion :traduccion"
	palabraTraducida = str(llamadaAPI('translate', 'translate', palabra))
	return palabraTraducida

def conjugacionVerbo(palabra):
	assert isinstance(palabra, str), "solo palabras porfavor ver funcion :conjugacionVerbo"
	palabraTraducida = str(llamadaAPI('translate', 'translate', palabra))
	conjugaciones = llamadaAPI('linguatool_conjugations', 'conjugate', palabraTraducida)
	conjugaciones_j = json.loads(conjugaciones)
	return conjugaciones_j

def wordApiMix(palabra):
	assert isinstance(palabra, str), "solo palabras porfavor ver funcion :wordApiMix"
	palabraTraducida = str(llamadaAPI('translate', 'translate', palabra))
	resultado = llamadaAPI('words_api', 'word', palabraTraducida)
	resFuncion_j = json.loads(resultado)

	definition = []
	partOfSpeech = []
	synonyms = []
	inCategory = []
	typeOf = []
	hasTypes = []
	verbGroup = []
	derivation = []
	examples = []

	if 'results' in resFuncion_j.keys():
		for x in range(0, len(resFuncion_j['results'])):
			for y in resFuncion_j['results'][x].keys():
				if y == 'definition':
					definition.append(resFuncion_j['results'][x][y])
				if y == 'partOfSpeech':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in partOfSpeech:
							partOfSpeech.append(resFuncion_j['results'][x][y][z])
				if y == 'synonyms':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in synonyms:
							synonyms.append(resFuncion_j['results'][x][y][z])
				if y == 'inCategory':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in inCategory:
							inCategory.append(resFuncion_j['results'][x][y][z])
				if y == 'typeOf':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in typeOf:
							typeOf.append(resFuncion_j['results'][x][y][z])
				if y == 'hasTypes':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in hasTypes:
							hasTypes.append(resFuncion_j['results'][x][y][z])
				if y == 'verbGroup':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in verbGroup:
							verbGroup.append(resFuncion_j['results'][x][y][z])
				if y == 'derivation':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in derivation:
							derivation.append(resFuncion_j['results'][x][y][z])
				if y == 'examples':
					for z in range(0, len(resFuncion_j['results'][x][y])):
						if resFuncion_j['results'][x][y][z] not in examples:
							examples.append(resFuncion_j['results'][x][y][z])
	else:
		definition = ['word not found']
		partOfSpeech = ['word not found']
		synonyms = ['word not found']
		inCategory = ['word not found']
		typeOf = ['word not found']
		hasTypes = ['word not found']
		verbGroup = ['word not found']
		derivation = ['word not found']
		examples = ['word not found']

	arrRes = {'definition' : definition ,
			  'partOfSpeech' : partOfSpeech,
			  'synonyms' : synonyms,
			 'inCategory' : inCategory,
			 'typeOf' : typeOf,
			 'hasTypes' : hasTypes,
			 'verbGroup' : verbGroup,
			 'derivation' : derivation,
			 'examples' : examples}
	return arrRes

def translatR(palabra):
	assert isinstance(palabra, str), "solo palabras porfavor ver funcion :translatR"
	palabraTraducida = str(llamadaAPI('translate', 'translate', palabra))
	translat = llamadaAPI('translatR', 'respuesta', palabraTraducida)
	translat_j = json.loads(translat)
	return translat_j

def apiDolphin(palabra):
	assert isinstance(palabra, str), "solo palabras porfavor ver funcion :apiDolphin"
	palabraTraducida = str(llamadaAPI('translate', 'translate', palabra))
	apiIngles = llamadaAPI('dolphinWords', 'buscarDic', palabraTraducida)
	return apiIngles




