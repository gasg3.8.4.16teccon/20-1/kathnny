<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny">
    <img src="Anexos/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Kathnny</h3>

  <p align="center">
    English API for the Construction Technologies course
    <br />
    <a href="https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny">View Demo</a>
    ·
    <a href="https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny/-/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Kathnny](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [Documentation](#documentation)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## Kathnny

<!--[![Product Name Screen Shot][product-screenshot]](https://example.com)-->

The Kathnny API aims to have functionality to perform translations and conjugations of words through the query and connection to other APIs, this including an API that stores information from a database, this in order to process such information to other APIs and generate return data through ours, we plan to create an interface client to make queries in a more user-friendly way.

![Pagina Principal](https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny/-/blob/master/Anexos/Portada.PNG)

### Built With
* [Flask python library](https://pypi.org/project/Flask/)
```
pip install Flask
```
* [Json python library](https://pypi.org/project/json5/)
```
pip install json
```
* [Flask_wtf python library](https://pypi.org/project/Flask-WTF/)
```
pip install flask_wtf
```
* [PyJWT python library](https://pyjwt.readthedocs.io/en/latest/)
```
pip install pyjwt
```
* [Flask-Limiter python library](https://pypi.org/project/Flask-Limiter/)
```
pip install Flask-Limiter
```


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* [Python](https://www.python.org/)
* [Pip](https://pypi.org/project/pip/)
```sh
python get-pip.py
```

### Installation

1. Clone the repo 
```sh
git@gitlab.com:gasg3.8.4.16teccon/20-1/kathnny.git
```
2. Get a free API Key at https://kathnny.com

<!-- USAGE EXAMPLES -->
## Usage
1. Send token to email (GET method)
```sh
http://127.0.0.1:5000/email
```
2. Get API response (GET method)
```sh
http://127.0.0.1:5000/conjugacion?palabra=
```

## Documentation
```sh
https://gitlab.com/gasg3.8.4.16teccon/20-1/kathnny/-/blob/master/Documentacion/Documentacion.pdf
```


<!-- ROADMAP 
## Roadmap

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a list of proposed features (and known issues).

-->

<!-- CONTRIBUTING 
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request
-->


<!-- CONTACT -->
## Contact

katherine Ccama Huallpa - kccamah@ulasalle.edu.pe
Kenny Mollapaza Morocco - kmollapazam@ulasalle.edu.pe

<!--Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)-->