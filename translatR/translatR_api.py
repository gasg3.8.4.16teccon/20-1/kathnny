import http.client

translatR = http.client.HTTPSConnection("ahidalgogulasalle.appspot.com")

def respuesta(palabra):
    assert isinstance(palabra, str), "solo palabras porfavor ver funcion :respuesta"
    ruta_completa = ("/translatR/API?input=" + palabra)
    translatR.request("GET", ruta_completa)
    respuesta = translatR.getresponse()
    data = respuesta.read()
    return data
