import Linguatools_Conjugations_key

import http.client

linguatools = http.client.HTTPSConnection("linguatools-conjugations.p.rapidapi.com")

def conjugate(verbo):
	assert isinstance(verbo,str),"solo palabras porfavor ver funcion :conjugate"
	ruta = "/conjugate/?verb="
	ruta_completa = (ruta + verbo)
	linguatools.request("GET", ruta_completa, headers=Linguatools_Conjugations_key.headers)
	respuesta = linguatools.getresponse()
	data = respuesta.read()
	return data
