import kathnny_respuesta

from flask import Flask, request, jsonify, render_template
from flask_restful import Resource, Api, reqparse
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import jwt
from functools import wraps
from flask_wtf import FlaskForm
from wtforms import StringField

import email, smtplib, ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import json

app = Flask(__name__)
api = Api(app)
app.config['SECRET_KEY'] = "12345678"
limiter = Limiter(
    app
)


class EmailForm(FlaskForm):
    email = StringField('email')


def decifrar():
    token = request.args.get('token')
    data = jwt.decode(token, 'secret', algorithms=['HS256'])
    return str(data.values())


def token_requerid(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return jsonify({'mensaje': 'Falta el token'}), 403
        try:
            data = jwt.decode(token, 'secret', algorithms=['HS256'])
        except:
            return jsonify({'mensaje': 'Token invalido'}), 403
        return f(*args, **kwargs)

    return decorated


@app.route('/conjugacion', methods=['GET'])
# @token_requerid
# @limiter.limit("20/day", exempt_when= lambda : decifrar() == "dict_values(['admin'])")
def getVerbo():  # Conbinacion de translate, linguatools y word api
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.conjugacionRespuesta(str(palabra))
	# return render_template('api.html', resp=data)
    return data


@app.route('/traduccion', methods=['GET'])
# @token_requerid
# @limiter.limit("20/day", exempt_when= lambda : decifrar() == "dict_values(['admin'])")
def getTraduccion():  # Translate
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.traduccion(str(palabra))
    return data


@app.route('/conjugacionVerbo', methods=['GET'])
# @token_requerid
# @limiter.limit("20/day", exempt_when= lambda : decifrar() == "dict_values(['admin'])")
def getConjugacion():  # Linguatools conjugations
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.conjugacionVerbo(str(palabra))
    return data


@app.route('/mix', methods=['GET'])
# @token_requerid
# @limiter.limit("20/day", exempt_when= lambda : decifrar() == "dict_values(['admin'])")
def getWordApi():
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.wordApiMix(str(palabra))
    return data
@app.route('/dolphin', methods=['GET'])
def getApiDolphin():
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.apiDolphin(str(palabra))
    return data
@app.route('/translate', methods=['GET'])
def getApiTraslateTr():
    palabra = request.args.get('palabra')
    data = kathnny_respuesta.translatR(str(palabra))
    return data

@app.route('/email', methods=['GET'])
def email():
    email = request.args.get('email')
    t = jwt.encode({str(email): 'user'}, 'secret', algorithm='HS256')
    subject = "Kathnny Token"
    body = t.decode('UTF-8')
    port = 465
    sender = "example@example.com"  # Email de origen
    password = "password"  # contrasenia de email de origen
    recieve = str(email)
    message = MIMEMultipart()
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))
    text = message.as_string()
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(sender, password)
        server.sendmail(sender, recieve, text)

    return "Email Enviado"

def email2(email):
    #email = request.args.get('email')
    t = jwt.encode({str(email): 'user'}, 'secret', algorithm='HS256')
    subject = "Kathnny Token"
    body = t.decode('UTF-8')
    port = 465
    sender = "apikathnny@gmail.com"  # Email de origen
    password = "kathnny0717"  # contrasenia de email de origen
    recieve = str(email)
    message = MIMEMultipart()
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))
    text = message.as_string()
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(sender, password)
        server.sendmail(sender, recieve, text)

    return "Email Enviado"


@app.route('/', methods=['GET','POST'])
def home():
	form=EmailForm()
	if form.validate_on_submit():
		a=format(form.email.data)
		email2(a)
	return render_template('index.html', form=form)


@app.route('/api')
def api():
    data = getVerbo()
    lt = getConjugacion()
    trans = getTraduccion()
    word = getWordApi()
    defi = word['definition'][0]
    return render_template('api.html', resp=data, ling=lt, trad=trans, wa=word, defini=defi)


def construir(key):
    cadena = ""
    wordsapip = getWordApi()
    if key == "partOfSpeech":
        for i in wordsapip[key]:
            cadena = cadena + i
    else:
        for i in wordsapip[key]:
            cadena = cadena +"➤ "+ i + "\n"
    return cadena


@app.route('/client')
def client():
    data = getVerbo()
    wordsapip = getWordApi()
    definition = construir("definition")
    antonym = wordsapip.keys()
    partOfSpeech = construir("partOfSpeech")  # ver
    synonyms = construir("synonyms")
    inCategory = construir("inCategory")
    typeOf = construir("typeOf")
    hasTypes = construir("hasTypes")
    verbGroup = construir("verbGroup")
    derivation = construir("derivation")
    examples = construir("examples")
    trans = getTraduccion()
    guri=getApiDolphin()
    aron=getApiTraslateTr()
    return render_template('client.html', microsoft=trans, resp1=data, defi=definition, antonimo=antonym,
                           partOf=partOfSpeech, sinonimo=synonyms, categoria=inCategory, tipo=typeOf, tipos=hasTypes,
                           Vergbs=verbGroup, derivacion=derivation, ejemplos=examples,guri=guri,aron=aron)


if __name__ == '__main__':
    app.run(debug=True)
	