import WordsAPI_key

import http.client

words_api = http.client.HTTPSConnection("wordsapiv1.p.rapidapi.com")

def hatchback(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hatchback"
	ruta_completa = ("/words/" + palabra + "/typeOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasTypes(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasTypes"
	ruta_completa = ("/words/" + palabra + "/hasTypes")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def partOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :partOf"
	ruta_completa = ("/words/" + palabra + "/partOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasParts(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasParts"
	ruta_completa = ("/words/" + palabra + "/hasParts")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def instanceOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :instanceOf"
	ruta_completa = ("/words/" + palabra + "/instanceOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasInstances(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasInstances"
	ruta_completa = ("/words/" + palabra + "/hasInstances")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def similarTo(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :similarTo"
	ruta_completa = ("/words/" + palabra + "/similarTo")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def also(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :also"
	ruta_completa = ("/words/" + palabra + "/also")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def entails(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :entails"
	ruta_completa = ("/words/" + palabra + "/entails")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def memberOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :memberOf"
	ruta_completa = ("/words/" + palabra + "/memberOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasMembers(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasMembers"
	ruta_completa = ("/words/" + palabra + "/hasMembers")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def substanceOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :substanceOf"
	ruta_completa = ("/words/" + palabra + "/substanceOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasSubstances(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasSubstances"
	ruta_completa = ("/words/" + palabra + "/hasSubstances")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def inCategory(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :inCategory"
	ruta_completa = ("/words/" + palabra + "/inCategory")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasCategories(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasCategories"
	ruta_completa = ("/words/" + palabra + "/hasCategories")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def usageOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :usageOf"
	ruta_completa = ("/words/" + palabra + "/usageOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def hasUsages(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :hasUsages"
	ruta_completa = ("/words/" + palabra + "/hasUsages")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def inRegion(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :inRegion"
	ruta_completa = ("/words/" + palabra + "/inRegion")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def regionOf(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :regionOf"
	ruta_completa = ("/words/" + palabra + "/regionOf")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def pertainsTo(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :pertainsTo"
	ruta_completa = ("/words/" + palabra + "/pertainsTo")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def word(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :word"
	ruta_completa = ("/words/" + palabra)
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def synonyms(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :synonyms"
	ruta_completa = ("/words/" + palabra + "/synonyms")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def definitions(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :definitions"
	ruta_completa = ("/words/" + palabra + "/definitions")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def antonyms(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :antonyms"
	ruta_completa = ("/words/" + palabra + "/antonyms")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def examples(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :examples"
	ruta_completa = ("/words/" + palabra + "/examples")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def rhymes(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :rhymes"
	ruta_completa = ("/words/" + palabra + "/rhymes")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def pronunciation(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :pronunciation"
	ruta_completa = ("/words/" + palabra + "/pronunciation")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def syllables(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :syllables"
	ruta_completa = ("/words/" + palabra + "/syllables")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

def frequency(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :frequency"
	ruta_completa = ("/words/" + palabra + "/frequency")
	words_api.request("GET", ruta_completa, headers=WordsAPI_key.headers)
	respuesta = words_api.getresponse()
	data = respuesta.read()
	return data

