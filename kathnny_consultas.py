import sys
sys.path.append("Linguatool_Conjugations")
sys.path.append("WordsApi")
sys.path.append("translate")
sys.path.append("translatR")
sys.path.append("DolphinWords")

from Linguatool_Conjugations.Linguatools_Conjugations_api import *
from WordsAPI.WordsAPI_api import *
from Translate.translate_api import *
from translatR.translatR_api import *
from DolphinWords.dolphinWords_api import *

wordsApiFuncs = {	'hatchback':hatchback, 
					'hasTypes':hasTypes, 
					'partOf': partOf,
					'hasParts': hasParts,
					'instanceOf': instanceOf,
					'hasInstances': hasInstances,
					'similarTo': similarTo,
					'also': also,
					'entails': entails,
					'memberOf': memberOf,
					'hasMembers': hasMembers,
					'substanceOf': substanceOf,
					'hasSubstances': hasSubstances,
					'inCategory': inCategory,
					'hasCategories': hasCategories,
					'usageOf': usageOf,
					'hasUsages': hasUsages,
					'inRegion': inRegion,
					'regionOf': regionOf,
					'pertainsTo': pertainsTo,
					'word': word,
					'synonyms': synonyms,
					'definitions': definitions,
					'antonyms': antonyms,
					'examples': examples,
					'rhymes': rhymes,
					'pronunciation': pronunciation,
					'syllables': syllables,
					'frequency': frequency
				}

linguatoolApiFuncs = {'conjugate': conjugate }

translateApiFuncs = {'translate': translate, 'detectLanguaje': detectLanguaje }

translatRApiFuncs = {'respuesta' : respuesta}

dolphinApiFuncs = {'buscarDic' : buscarDic}

def llamadaAPI(nombreApi, funcionApi, palabra):
	assert isinstance(palabra,str),("solo palabras porfavor ver funcion : " + funcionApi)
	
	if nombreApi == 'words_api':
		if wordsApiFuncs[funcionApi]:
			return wordsApiFuncs[funcionApi](palabra)

	elif nombreApi == 'linguatool_conjugations':
		if linguatoolApiFuncs[funcionApi]:
			return linguatoolApiFuncs[funcionApi](palabra)

	elif nombreApi == 'translate':
		if translateApiFuncs[funcionApi]:
			return translateApiFuncs[funcionApi](palabra)

	elif nombreApi == 'translatR':
		if translatRApiFuncs[funcionApi]:
			return translatRApiFuncs[funcionApi](palabra)

	elif nombreApi == 'dolphinWords':
		if dolphinApiFuncs[funcionApi]:
			return dolphinApiFuncs[funcionApi](palabra)





