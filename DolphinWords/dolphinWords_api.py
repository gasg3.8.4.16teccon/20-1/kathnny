import http.client

dolphin = http.client.HTTPSConnection("peluquerialenin.com")

def buscarDic(palabra):
    assert isinstance(palabra, str), "solo palabras porfavor ver funcion :respuesta"
    ruta_completa = ('/search.php?word="' + palabra + '"')
    dolphin.request("GET", ruta_completa)
    respuesta = dolphin.getresponse()
    data = str(respuesta.read())
    if "0 results" in data:
        return "No esta presente en el diccionario"
    else:
        a = data.index('Description:')
        c = data.index('<br>Source:')
        b = a + 13
        return data[b:c]



