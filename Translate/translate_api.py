import translate_key
import http.client
import xmltodict

translate_micro = http.client.HTTPSConnection("microsoft-azure-translation-v1.p.rapidapi.com")

def translate(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :conjugate"
	ruta = "/translate?from=es&to=en&text="
	ruta_completa = (ruta + palabra)
	translate_micro.request("GET", ruta_completa, headers=translate_key.headers)
	respuesta = translate_micro.getresponse()
	data = respuesta.read()
	return xmltodict.parse(data)['string']['#text']

def detectLanguaje(palabra):
	assert isinstance(palabra,str),"solo palabras porfavor ver funcion :conjugate"
	ruta = "/Detect?text="
	ruta_completa = (ruta + palabra)
	translate_micro.request("GET", ruta_completa, headers=translate_key.headers)
	respuesta = translate_micro.getresponse()
	data = respuesta.read()
	return xmltodict.parse(data)['string']['#text']
